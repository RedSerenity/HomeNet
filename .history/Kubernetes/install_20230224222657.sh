#!/bin/bash

KUBECTL=kubectl

case $1 in
	dashboard)
		$KUBECTL apply -f Dashboard/aio/deploy/recommended.yaml
		;;
	dashboard-auth)
		$KUBECTL apply -f Dashboard/andernet/auth.yaml
		;;
	dashboard-noauth)
		$KUBECTL apply -f Dashboard/andernet/noauth.yaml
		;;
	*)
		echo "Invalid Option"
		;;
esac
