#!/bin/bash

KUBECTL=kubectl

case $1 in
	dashboard)
		$KUBECTL apply -f Dashboard/aio/deploy/recommended.yaml
		;;
	*)
		echo "Invalid Option"
		;;
esac
