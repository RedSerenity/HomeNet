#!/bin/bash

KUBECTL=kubectl
K_APPLY=$KUBECTL apply -f

case $1 in
	dashboard)
		$K_APPLY Dashboard/aio/deploy/recommended.yaml
		;;
	dashboard-auth)
		$K_APPLY Dashboard/andernet/auth.yaml
		;;
	dashboard-noauth)
		$K_APPLY Dashboard/andernet/noauth.yaml
		;;
	postgres)
		$K_APPLY Postgres/kubegres.yaml
		$K_APPLY Postgres/andernet/postgres.yaml
		;;
	*)
		echo "Invalid Option"
		exit -1
		;;
esac