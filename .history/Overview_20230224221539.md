# Network for Kingdom of Ander

## System Required Software
* [Docker](https://docs.docker.com/)

## Required Services
* [Hashicorp Vault](https://www.vaultproject.io/)
* [KeyCloak](https://www.keycloak.org/)
  * [PostgreSQL Database](https://www.postgresql.org/)
* [Portainer](https://www.portainer.io/)
* [Caddy Server](https://caddyserver.com/)

## Network Services
* [Bind DNS](https://www.isc.org/bind/)
* [Kea DHCPd](https://www.isc.org/kea/)
  * [PostgreSQL Database](https://www.postgresql.org/)
  * [Stork UI](https://gitlab.isc.org/isc-projects/stork)
* Content Filter
* UniFi Controller
  * MongoDB

## Extra Services
* NetBox
* CodeServer



cat <<EOF | kubectl -n kubernetes-dashboard create -f -
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
  labels:
    k8s-app: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
EOF

https://kevinholditch.co.uk/2022/02/18/running-unifi-controller-on-home-k8s-cluster-with-metallb/

https://medium.com/@reefland/migrating-unifi-network-controller-from-docker-to-kubernetes-5aac8ed8da76

https://devopscube.com/deploy-mongodb-kubernetes/

https://github.com/mongodb/mongodb-kubernetes-operator




#apiVersion: v1
#kind: Namespace
#metadata:
#  labels:
#    control-plane: controller-manager
#  name: db-postgres
---
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: sc-postgres
provisioner: driver.longhorn.io
allowVolumeExpansion: true
reclaimPolicy: Delete
parameters:
  numberOfReplicas: "1"
  dataLocality: "strict-local"
  staleReplicaTimeout: "60" # 48 hours in minutes
  fromBackup: ""
  fsType: "ext4"
---
apiVersion: v1
kind: Secret
metadata:
  name: postgres-secret
  namespace: kubegres-system
type: Opaque
stringData:
  superUserPassword: 1203*f9OzERdCCwvVfkn3GMO
  replicationUserPassword: 5^ttH*2gKlstTfUwu@s9aQZL
---
apiVersion: kubegres.reactive-tech.io/v1
kind: Kubegres
metadata:
  name: postgres
  namespace: kubegres-system
spec:
   replicas: 2
   image: postgres:14.1
   database:
      size: 4Gi
      storageClassName: sc-postgres
   env:
      - name: POSTGRES_PASSWORD
        valueFrom:
           secretKeyRef:
              name: postgres-secret
              key: superUserPassword
      - name: POSTGRES_REPLICATION_PASSWORD
        valueFrom:
           secretKeyRef:
              name: postgres-secret
              key: replicationUserPassword