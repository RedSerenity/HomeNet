#!/bin/bash

KUBECTL="kubectl"
K_APPLY="$KUBECTL apply"
K_CREATE="$KUBECTL create"

case $1 in
	longhorn)
		$K_APPLY -f Longhorn/deploy/longhorn.yaml
		$K_APPLY -f Longhorn/andernet/longhorn.yaml
		;;
	metallb)
		$K_APPLY -f MetalLb/config/manifests/metallb-native.yaml
		$K_APPLY -f MetalLb/andernet/metallb.yaml
		;;
	haproxy)
		$K_APPLY -f HAProxy/deploy/haproxy-ingress.yaml
		;;
	dashboard)
		$K_APPLY -f Dashboard/aio/deploy/recommended.yaml
		;;
	dashboard-auth)
		$K_APPLY -f Dashboard/andernet/auth.yaml
		;;
	dashboard-noauth)
		$K_APPLY -f Dashboard/andernet/noauth.yaml
		;;
	postgres)
		$K_APPLY -f Postgres/kubegres.yaml
		$K_APPLY -f Postgres/andernet/postgres.yaml
		;;
	mongodb)
		$K_APPLY -f MongoDb/deploy/clusterwide/
		$K_APPLY -f MongoDb/config/crd/bases/mongodbcommunity.mongodb.com_mongodbcommunity.yaml
		$K_APPLY -f MongoDb/andernet/init.yaml
		$K_APPLY -k MongoDb/config/rbac/ --namespace mongodb-system
		$K_CREATE -f MongoDb/config/manager/manager.yaml --namespace mongodb-system
		$K_APPLY -f MongoDb/andernet/mongodb.yaml
		;;
	*)
		echo "Invalid Option"
		exit 1
		;;
esac