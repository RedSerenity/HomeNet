# Network for Kingdom of Ander

## System Required Software
* [Docker](https://docs.docker.com/)

## Required Services
* [Hashicorp Vault](https://www.vaultproject.io/)
* [KeyCloak](https://www.keycloak.org/)
  * [PostgreSQL Database](https://www.postgresql.org/)
* [Portainer](https://www.portainer.io/)
* [Caddy Server](https://caddyserver.com/)

## Network Services
* [Bind DNS](https://www.isc.org/bind/)
* [Kea DHCPd](https://www.isc.org/kea/)
  * [PostgreSQL Database](https://www.postgresql.org/)
  * [Stork UI](https://gitlab.isc.org/isc-projects/stork)
* Content Filter
* UniFi Controller
  * MongoDB

## Extra Services
* NetBox
* CodeServer


# Steps to Install Cluster

## Install Alpine Linux
* Install Alpine Linux
  * Edit /etc/apk/repositories
    * sed -i -r 's;^#http(.*main|.*community)$;http\1;' /etc/apk/repositories
  * apk update
  * apk upgrade
  * apk add bind-tools curl findmnt grep awk blkid lsblk open-iscsi
* Update Alpine Configuration
  * sed -i -r 's;(/\s+ext[2-4].*rw,relatime)\s;\1,rshared ;' /etc/fstab
  * echo "cgroup /sys/fs/cgroup cgroup defaults 0 0" >> /etc/fstab
  * sed -i -r 's;#(rc_cgroup_mode=")(hybrid)(");\1unified\3;' /etc/rc.conf
  * reboot
* Install CoreDNS (Currently keeps crashing!)
  * apk add coredns
    * rc-update add coredns
    * Create Corefile (/etc/coredns/)
    * Enable Healthchecks (/etc/conf.d/coredns)
    * Modify /etc/resolv.conf


## K3S
* Install K3S on Controller
  * Download Install Script
    * curl -sfL https://get.k3s.io | sed 's/sourcex/source/g' > install.sh
	* Run Installer
  	* K3S_TOKEN=1234567890 sh install.sh server --cluster-init --disable=traefik,servicelb
		* sed -i '/cluster-init/d' /etc/init.d/k3s
		* reboot


# Notes

```
Install MetalLB
	-> Configure IPAddressPool(s)
	-> Configure L2

Install HAProxy
	-> Update service from ClusterIP/NodePort to LoadBalancer
```




https://technekey.com/how-to-setup-longhorn-block-storage-for-kubernetes-cluster/

https://kevinholditch.co.uk/2022/02/18/running-unifi-controller-on-home-k8s-cluster-with-metallb/

https://medium.com/@reefland/migrating-unifi-network-controller-from-docker-to-kubernetes-5aac8ed8da76

https://devopscube.com/deploy-mongodb-kubernetes/

https://github.com/mongodb/mongodb-kubernetes-operator

https://computingforgeeks.com/deploy-metallb-load-balancer-on-kubernetes/

https://medium.com/geekculture/bare-metal-kubernetes-with-metallb-haproxy-longhorn-and-prometheus-370ccfffeba9