#!/bin/sh

runCmd () {
	local DESC="$1"
	shift
	local CMD="$@"
	echo $DESC

	if $CMD ; then
		echo -e "\t[SUCCESS]"
	else
		echo -e "\t[ERROR]"
	fi
}

runCmd \
	"Enabling APK Repositories" \
	sed -i -r 's;^#http(.*main|.*community)$;http\1;' /etc/apk/repositories

runCmd \
	"Update APK Repositories" \
	apk update

runCmd \
	"Upgrade System" \
	apk upgrade

runCmd \
	"Install Required Packages" \
	apk add bind-tools curl findmnt grep blkid lsblk open-iscsi git

runCmd \
	"Enable ISCId"
	rc-update add iscsid

runCmd \
	"Enable CGROUP Unified mode"
	sed -i -r 's;#(rc_cgroup_mode=")(hybrid)(");\1unified\3;' /etc/rc.conf

runCmd \
	"Enable CGROUPs"
	rc-update add cgroups

runCmd \
	"Update 'fstab' / propagation"
	sed -i -r 's;(/\s+ext[2-4].*rw,relatime)\s;\1,rshared ;' /etc/fstab

echo "Please reboot."