#!/bin/sh

case $1 in
	controller)
		curl -sfL https://get.k3s.io | \
			sed 's/sourcex/source/g' | \
			K3S_TOKEN=1234567890 sh -s - server --cluster-init --disable=traefik,servicelb
		
		sed -i '/cluster-init/d' /etc/init.d/k3s
	;;
	worker)
		if [ "$2" = "" ]; then
			echo "Missing ip address"
			echo "$0 worker <controller ip>"
			exit -1
		fi

		curl -sfL https://get.k3s.io | \
			sed 's/sourcex/source/g' | \
			K3S_TOKEN=1234567890 K3S_URL=https://${2}/ sh -s - agent
	;;
	*)
		echo "$0 controller"
		echo -e "\t or"
		echo "$0 worker <controller ip>"
esac